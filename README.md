# Fampedia

## Status
[![Netlify Status](https://api.netlify.com/api/v1/badges/a72c4645-2ef7-4cce-ac81-732f691f1813/deploy-status)](https://app.netlify.com/sites/competent-cray-989081/deploys)
![coverage](https://gitlab.com/philipOE/fampedia/badges/dev/coverage.svg)


## Setup
Setup documentation can be found [here](Docs/Setup.md)